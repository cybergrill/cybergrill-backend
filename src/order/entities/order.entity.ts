import { Address, Additionals, Hamburger, Drinks } from '@prisma/client';

export class Order {
  id: number;
  customer: string;
  total: number;
  orderAdditionals: {
    additional: Additionals;
    quantity: number;
  }[];
  orderHamburgers: {
    hamburger: Hamburger;
    quantity: number;
  }[];
  orderDrinks: {
    drink: Drinks;
    quantity: number;
  }[];
  address: Address;
  isClosed: boolean;
  isActive: boolean;
}
