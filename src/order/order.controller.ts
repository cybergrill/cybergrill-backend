import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Put,
} from '@nestjs/common';
import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { IsPublic } from 'src/auth/decorators/is-public.decorator';
import { ItemType } from 'src/utils/enums';


@Controller('order')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}


  @Post()
  @IsPublic()
  create(@Body() createOrderDto: CreateOrderDto) {
    return this.orderService.create(createOrderDto);
  }
  

  @Get('actives')
  @IsPublic()
  findAllActives() {
    return this.orderService.findAllActives();
  }

  @Get()
  @IsPublic()
  findAll() {
    return this.orderService.findAll();
  }


  @Get(':id')
  @IsPublic()
  findOne(@Param('id') id: string) {
    return this.orderService.findOne(+id);
  }

  @Patch(':id')
  @IsPublic()
  update(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
    return this.orderService.update(+id, updateOrderDto);
  }

  @Put(':id/delete')
  @IsPublic()
  remove(@Param('id') id: string) {
    return this.orderService.remove(+id);
  }

  @Post(':id/items')
  @IsPublic()
  async addItemToOrder(
    @Param('id') orderId: number,
    @Body('itemId') itemId: number,
    @Body('quantity') quantity: number,
    @Body('itemType') itemType: ItemType,
  ) {
    return this.orderService.addItensToOrder(orderId, itemId, quantity, itemType);
  }

  @Delete(':id/items/:itemId')
  @IsPublic()
  async removeItemFromOrder(
    @Param('id') orderId: number,
    @Param('itemId') itemId: number,
    @Body('itemType') itemType: ItemType,
  ) {
    return this.orderService.removeItemFromOrder(orderId, itemId, itemType);
  }

  @Put(':id/close')
  @IsPublic()
  async closeOrder(@Param('id') orderId: number) {
    return this.orderService.closeOrder(orderId);
  }

  @Put(':id/reopen')
  @IsPublic()
  async reopenOrder(@Param('id') orderId: number) {
    return this.orderService.reopenOrder(orderId);
  }
  
}
