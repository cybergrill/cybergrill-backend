import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateOrderAdditionalsDto {
  @IsNotEmpty()
  @IsNumber()
  quantity: number;

  @IsNotEmpty()
  @IsNumber()
  additionalId: number;
}
