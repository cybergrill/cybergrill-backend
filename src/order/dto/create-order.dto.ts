import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { CreateAddressDto } from 'src/address/dto/create-address.dto';
import { CreateOrderHamburgerDto } from './create-orderHamburger.dto';
import { CreateOrderAdditionalsDto } from './create-orderAdditionals.dto';
import { CreateorderDrinksDto } from './create-orderDrinks.dto';

export class CreateOrderDto {
  @IsString()
  customer: string;

  @IsNumber()
  @IsOptional()
  total: number;

  @ValidateNested()
  @Type(() => CreateAddressDto)
  address: CreateAddressDto;

  @ValidateNested({ each: true })
  @Type(() => CreateOrderAdditionalsDto)
  orderAdditionals: CreateOrderAdditionalsDto[];

  @ValidateNested({ each: true })
  @Type(() => CreateOrderHamburgerDto)
  orderHamburgers: CreateOrderHamburgerDto[];

  @ValidateNested({ each: true })
  @Type(() => CreateorderDrinksDto)
  orderDrinks: CreateorderDrinksDto[];

  @IsOptional()
  @IsBoolean()
  isClosed: boolean;

}
