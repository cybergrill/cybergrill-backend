import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateorderDrinksDto {
  @IsNotEmpty()
  @IsNumber()
  quantity: number;

  @IsNotEmpty()
  @IsNumber()
  drinkId: number;
}