import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateOrderHamburgerDto {
  @IsNotEmpty()
  @IsNumber()
  quantity: number;

  @IsNotEmpty()
  @IsNumber()
  hamburgerId: number;
}