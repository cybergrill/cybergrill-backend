import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto} from './dto/update-order.dto';
import { PrismaClient } from '.prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { Hamburger } from 'src/hamburgers/entities/hamburger.entity';
import { ItemType, UserRole } from 'src/utils/enums';


const prisma = new PrismaClient();

@Injectable()
export class OrderService {
  constructor(private readonly prisma: PrismaService) {}
  async create(createOrderDto: CreateOrderDto): Promise<any> {
    const { customer, total, address, orderAdditionals, orderHamburgers, orderDrinks} = createOrderDto;

    const order = await prisma.order.create({
      data: {
        customer,
        total,
        address: {
          create: address,
        },
        orderAdditionals: orderAdditionals? {
          create: orderAdditionals.map((item) => ({
            quantity: item.quantity,
            additional: { connect: { id: item.additionalId} },
          })),
        }: undefined,
        orderHamburgers: orderHamburgers? {
          create: orderHamburgers.map((item) => ({
            quantity: item.quantity,
            hamburger: { connect: { id: item.hamburgerId} },
          })),
        }: undefined,
        orderDrinks: orderDrinks? {
          create: orderDrinks.map((item) => ({
            quantity: item.quantity,
            drink: { connect: { id: item.drinkId} },
          })),
        }: undefined,
      },
      include: {
        address: true,
        orderAdditionals: {
          include: {
            additional: true,
          },
        },
        orderHamburgers: {
          include: {
            hamburger: true,
          },
        },
        orderDrinks: {
          include: {
            drink: true,
          },
        },
      },
    });
    return order;
  }

  async findAllActives() {
    const orders = await this.prisma.order.findMany({
      where: { isActive: true},
      orderBy: { modifiedAt: 'desc' },
      include: {
        orderHamburgers: {
          include: {
            hamburger: true,
          },
        },
        orderAdditionals: {
          include:{
            additional: true
          },
        },
        orderDrinks: {
          include:{
            drink: true
          },
        },
      },
    });
    return orders;
  }

  async findAll() {
    const orders = await this.prisma.order.findMany({
      orderBy: { modifiedAt: 'desc' },
      include: {
        address: true,
        orderHamburgers: true,
        orderAdditionals: true, 
        orderDrinks: true
      },
    });
    return orders;
  }


  async findOne(id: number) {
    const order = await prisma.order.findUnique({
      where: {
        id,
      },
      include: {
        orderHamburgers: {
          include: {
            hamburger: true,
          },
        },
        orderAdditionals: {
          include: {
            additional: true,
          },
        },
        orderDrinks: {
          include: {
            drink: true,
          },
        },
      },
    });
    if (order != null && !order.isActive) {
      throw new HttpException(`Order with id ${id} is not active`, HttpStatus.BAD_REQUEST);
    }
    return order;
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    const order = await this.prisma.order.findUnique({ where: { id }, include: { address: true } });
    
    if (!order) 
      throw new HttpException(`Order with id ${id} not found`, HttpStatus.NOT_FOUND);
    if (order.isclosed) 
      throw new HttpException('Order is already closed', HttpStatus.BAD_REQUEST);
    if (!order.isActive) 
      throw new HttpException('Order is not active', HttpStatus.BAD_REQUEST);

    const { address, customer } = updateOrderDto;
    const updateOrder = await this.prisma.order.update({
      where: { id },
      data: {
        customer: customer,
        address: {
          update: address
        }
      },
      include: { address: true },
    });
    return updateOrder;
  }

  async remove(id: number) {
    try {
      await prisma.order.update({
        where: { id },
        data: { isActive: false},
      });
      return;
    } catch (error) {
      console.log(error);
      throw new HttpException('Could not delete order', HttpStatus.NOT_FOUND);
    }
  }

  private validateItemType(itemType: ItemType): void {
    if (!itemType || !(Object.values(ItemType).includes(itemType))) {
      throw new HttpException('Tipo de item inválido.', HttpStatus.BAD_REQUEST);
    }
  }
  async addItensToOrder(orderId: number, itemId: number, quantity: number, itemType: ItemType){
    this.validateItemType(itemType);
    const order = await prisma.order.findUnique({
      where: { id: orderId },
      include: { 
        orderHamburgers: true,
        orderAdditionals: true,
        orderDrinks: true
      },
    });
    if (!order) {
      throw new HttpException('Order not found', HttpStatus.NOT_FOUND);
    }
    if (order.isclosed) {
      throw new HttpException('The order has already been closed', HttpStatus.BAD_REQUEST);
    }
    if(itemType === ItemType.Hamburger){
      const orderHamburger = order.orderHamburgers.find((oh) => oh.hamburgerId === itemId);
      if (orderHamburger) {
        await this.prisma.orderHamburger.update({
          where: { orderId_hamburgerId: { orderId, hamburgerId: itemId } },
          data: { quantity: orderHamburger.quantity + quantity },
        });
      } else {
        await this.prisma.orderHamburger.create({
          data: {
            quantity,
            hamburger: { connect: { id: itemId } },
            order: { connect: { id: orderId } },
          },
        });
      }
    }if(itemType === ItemType.Additional){
      const orderAdditional = order.orderAdditionals.find((oa) => oa.additionalId === itemId);
      if (orderAdditional) {
        await this.prisma.orderAdditional.update({
          where: { orderId_additionalId: { orderId, additionalId: itemId } },
          data: { quantity: orderAdditional.quantity + quantity },
        });
      } else {
        await this.prisma.orderAdditional.create({
          data: {
            quantity,
            additional: { connect: { id: itemId } },
            order: { connect: { id: orderId } },
          },
        });
      }
    } else if (itemType === ItemType.Drink) {
      const orderDrink = order.orderDrinks.find((od) => od.drinkId === itemId);
      if (orderDrink) {
        await this.prisma.orderDrinks.update({
          where: { orderId_drinkId: { orderId, drinkId: itemId } },
          data: { quantity: orderDrink.quantity + quantity },
        });
      } else {
        await this.prisma.orderDrinks.create({
          data: {
            quantity,
            drink: { connect: { id: itemId } },
            order: { connect: { id: orderId } },
          },
        });
      }
    }
    return prisma.order.findUnique({
      where: { id: orderId },
      include: {
        orderHamburgers: {
          include: { hamburger: true },
        },
        address: true,
        orderAdditionals: {
          include: { additional: true},
        },
        orderDrinks:{
          include: { drink: true}
        }
      },
    });
  }
  
  async removeItemFromOrder(orderId: number, itemId: number, itemType: ItemType) {
    this.validateItemType(itemType);
    const order = await this.prisma.order.findUnique({
      where: { id: orderId },
      include: {
        orderHamburgers: true,
        orderAdditionals: true,
        orderDrinks: true, 
      },
    });
    if (!order) {
      throw new HttpException('Order not found', HttpStatus.NOT_FOUND);
    }
    if (order.isclosed) {
      throw new HttpException('The order has already been closed', HttpStatus.BAD_REQUEST);
    }

    if (itemType === ItemType.Hamburger) {
      const orderHamburger = order.orderHamburgers.find((oh) => oh.hamburgerId === itemId);
      if (!orderHamburger) {
        throw new HttpException(`Hamburger with id ${itemId} not found in the order.`, HttpStatus.BAD_REQUEST);
      }
      if (orderHamburger.quantity === 1) {
        await this.prisma.orderHamburger.delete({
          where: { orderId_hamburgerId: { orderId, hamburgerId: itemId } },
        });
      } else {
        await this.prisma.orderHamburger.update({
          where: { orderId_hamburgerId: { orderId, hamburgerId: itemId } },
          data: { quantity: orderHamburger.quantity - 1 },
        });
      }
    } else if (itemType === ItemType.Additional) {
      const orderAdditional = order.orderAdditionals.find((oa) => oa.additionalId === itemId);
      if (!orderAdditional) {
        throw new HttpException(`Additional with id ${itemId} not found in order.`, HttpStatus.BAD_REQUEST);
      }
      if (orderAdditional.quantity === 1) {
        await this.prisma.orderAdditional.delete({
          where: { orderId_additionalId: { orderId, additionalId: itemId } },
        });
      } else {
        await this.prisma.orderAdditional.update({
          where: { orderId_additionalId: { orderId, additionalId: itemId } },
          data: { quantity: orderAdditional.quantity - 1 },
        });
      }
    }else if (itemType === ItemType.Drink) {
      const orderDrink = order.orderDrinks.find((od) => od.drinkId === itemId);
      if (!orderDrink) {
        throw new HttpException(`Drink with id ${itemId} not found in order.`, HttpStatus.BAD_REQUEST);
      }
      if (orderDrink.quantity === 1) {
        await this.prisma.orderDrinks.delete({
          where: { orderId_drinkId: { orderId, drinkId: itemId } },
        });
      } else {
        await this.prisma.orderDrinks.update({
          where: { orderId_drinkId: { orderId, drinkId: itemId } },
          data: { quantity: orderDrink.quantity - 1 },
        });
      }
    }
  
    return this.prisma.order.findUnique({
      where: { id: orderId },
      include: {
        orderHamburgers: { include: { hamburger: true } },
        address: true,
        orderAdditionals: { include: { additional: true } },
        orderDrinks: { include: { drink: true } },
      },
    });
  }

  async closeOrder(orderId: number){
    const order = await this.prisma.order.findUnique({
      where: { id: orderId },
      include: {
        orderHamburgers: { include: { hamburger: true } },
        address: true,
        orderAdditionals: { include: { additional: true } },
        orderDrinks: { include: { drink: true } },
      },
    });

    if (!order) {
      throw new HttpException('Order not found', HttpStatus.NOT_FOUND);
    }

    if (order.isclosed) {
      throw new HttpException('The order has already been closed', HttpStatus.BAD_REQUEST);
    }

    if (!order.orderHamburgers.length && !order.orderAdditionals.length && !order.orderDrinks.length) {
      throw new HttpException('The order cannot be closed as it has no items', HttpStatus.BAD_REQUEST);
    }
  
    const total = await this.calculateTotal(orderId);
    const whatsappUrl = await this.getWhatsAppLink(orderId, total);

    const updateClosedOrder = await this.prisma.order.update({
      where: { id: orderId },
      include: {
        orderHamburgers: { include: { hamburger: true } },
        address: true,
        orderAdditionals: { include: { additional: true } },
        orderDrinks: { include: { drink: true } },
      },
      data: { total, isclosed: true },
    });

    return {
      message: 'Order closed successfully!',
      updateClosedOrder,
      whatsappUrl: whatsappUrl,
    };
  }

  async reopenOrder(orderId: number) {
    const order = await this.prisma.order.findUnique({ where: { id: orderId } });

    if (!order) {
      throw new HttpException('Order not found', HttpStatus.NOT_FOUND);
    }

    if (!order.isclosed) {
      throw new HttpException('The order is already open', HttpStatus.BAD_REQUEST);
    }

    return this.prisma.order.update({
      where: { id: orderId },
      data: { isclosed: false },
    });
  }

  async getOwnerUser(){
    const owner = await this.prisma.user.findFirst({
      where: {
        role: UserRole.OWNER
      }
    });
    return owner;
  }
  private async  getWhatsAppLink(ObjectId: Number, total: Number) {
    const owner = await this.getOwnerUser();
    let message = "";
    if(owner != null){
      const ownerPhoneNumber = `55${owner.phone}`;
      message = `Olá, eu acabei de fazer o pedido número ${ObjectId} no valor de R$${total}`;
      message = `https://wa.me/${ownerPhoneNumber}?text=${encodeURIComponent(message)}`;
    }else{
      message = "Não foi possível encontrar um usuário do tipo owner para enviar a mensagem.";
    }

    return message;
  }
  
  private async calculateTotal(orderId: number){
    const orderWithDetails = await this.prisma.order.findUnique({
      where: { id: orderId },
      include: {
        orderHamburgers: { include: { hamburger: true } },
        orderAdditionals: { include: { additional: true } },
        orderDrinks: { include: { drink: true } },
      },
    });
  
    let total = 0;
  
    if(orderWithDetails != null){
      for (const hamburger of orderWithDetails.orderHamburgers) {
        total += hamburger.hamburger.price * hamburger.quantity;
      }

      for (const additional of orderWithDetails.orderAdditionals) {
        total += additional.additional.price * additional.quantity;
      }

      for (const drink of orderWithDetails.orderDrinks) {
        total += drink.drink.price * drink.quantity;
      }
    }
  
    return total;
  }
}

