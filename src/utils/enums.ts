export enum ItemType {
  Hamburger = 'hamburger',
  Additional = 'additional',
  Drink = 'drink', 
}

export enum UserRole {
  ADMIN = 'admin',
  CUSTOMER = 'customer',
  OWNER = 'owner',
}