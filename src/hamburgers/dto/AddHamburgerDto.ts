import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class AddHamburgerDto {
  @ApiProperty({ required: true })
  @IsNumber()
  @IsNotEmpty()
  hamburgerId: number;

  @ApiProperty({ required: true })
  @IsNumber()
  @IsNotEmpty()
  quantity: number;
}