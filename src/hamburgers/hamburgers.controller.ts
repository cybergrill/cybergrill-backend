import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { HamburgersService } from './hamburgers.service';
import { CreateHamburgerDto } from './dto/create-hamburger.dto';
import { UpdateHamburgerDto } from './dto/update-hamburger.dto';
import { IsPublic } from 'src/auth/decorators/is-public.decorator';
import { ApiTags } from '@nestjs/swagger';
import { Query } from '@nestjs/common';

@ApiTags('Hamburgers')
@Controller('hamburgers')
@IsPublic()
export class HamburgersController {
  constructor(private readonly hamburgersService: HamburgersService) {}

  @Post()
  @IsPublic()
  create(@Body() createHamburgerDto: CreateHamburgerDto) {
    return this.hamburgersService.create(createHamburgerDto);
  }

  @Get()
  @IsPublic()
  findAll(@Query('name') name: string) {
    return this.hamburgersService.findAll(name);
  }

  @Get(':id')
  @IsPublic()
  findOne(@Param('id') id: string) {
    return this.hamburgersService.findOne(+id);
  }

  @Patch(':id')
  @IsPublic()
  update(
    @Param('id') id: string,
    @Body() updateHamburgerDto: UpdateHamburgerDto,
  ) {
    return this.hamburgersService.update(+id, updateHamburgerDto);
  }

  @Delete(':id')
  @IsPublic()
  remove(@Param('id') id: string) {
    return this.hamburgersService.remove(+id);
  }
}
