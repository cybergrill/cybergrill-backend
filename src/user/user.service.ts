import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserRole } from 'src/utils/enums';

@Injectable()
export class UserService {
  constructor(private readonly prisma: PrismaService) {}
  async create(createUserDto: CreateUserDto) {
    const { role, ...user} = createUserDto;
    user.password = await bcrypt.hash(user.password, 10);

    const existingOwner = await this.prisma.user.findFirst({
      where: { role: 'owner' },
    });
  
    if (role === UserRole.OWNER && existingOwner) {
      throw new HttpException('Já existe um usuário com a role de owner', HttpStatus.BAD_REQUEST);
    }

    const createUser = await this.prisma.user.create({
      data: {
        ...user,
        role: role,
      },
      select: {
        id: true,
        name: true,
        password: false,
        phone: true,
        username: true,
        role: true,
      },
    });

    return createUser;
  }

  async findAll() {
    const users = await this.prisma.user.findMany({
      select: {
        id: true,
        name: true,
        password: false,
        phone: true,
        username: true,
        role: true,
      },
    });
    return users.map((user) => user);
  }

  async findOne(id: number) {
    const user = await this.prisma.user.findUnique({
      where: { id },
      select: {
        id: true,
        name: true,
        password: false,
        phone: true,
        username: true,
        role: true,
      },
    });
    return user;
  }

  async findByUsername(username: string) {
    return await this.prisma.user.findUnique({ where: { username } });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const updateUser = await this.prisma.user.update({
      where: { id },
      data: updateUserDto,
      select: {
        id: true,
        name: true,
        password: false,
        phone: true,
        username: true,
        role: true,
      },
    });

    return updateUser;
  }

  async remove(id: number) {
    try {
      await this.prisma.user.delete({
        where: {
          id,
        },
      });
      return;
    } catch (error) {
      throw new HttpException('Usuário não encontrado', HttpStatus.NOT_FOUND);
    }
  }
}
