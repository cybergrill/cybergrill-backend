import { Type } from 'class-transformer';
import {
  IsDefined,
  IsEnum,
  IsString,
  Matches,
  MaxLength,
  MinLength,
  ValidateNested,
} from 'class-validator';
import { CreateAddressDto } from 'src/address/dto/create-address.dto';
import { UserRole } from 'src/utils/enums';

export class CreateUserDto {
  @IsString()
  username: string;

  @IsString()
  name: string;

  @IsString()
  @MinLength(6)
  @MaxLength(20)
  password: string;

  @IsString()
  @Matches(/^[0-9]{11}$/, { message: 'O telefone deve ter 11 dígitos numéricos.' })
  phone?: string;

  @IsEnum(UserRole, { message: 'A role deve ser uma das opções disponíveis.' })
  role: UserRole;
}
