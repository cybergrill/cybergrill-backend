import { Address } from 'src/address/entities/address.entity';
import { UserRole } from 'src/utils/enums';

export class User {
  id: number;
  username: string;
  name: string;
  password: string;
  phone?: string;
  roles: UserRole;
}


