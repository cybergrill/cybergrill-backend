/*
  Warnings:

  - You are about to drop the column `costomer` on the `Order` table. All the data in the column will be lost.
  - You are about to drop the column `orderId` on the `User` table. All the data in the column will be lost.
  - Added the required column `customer` to the `Order` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "User" DROP CONSTRAINT "User_orderId_fkey";

-- DropIndex
DROP INDEX "User_orderId_key";

-- AlterTable
ALTER TABLE "Order" DROP COLUMN "costomer",
ADD COLUMN     "customer" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "User" DROP COLUMN "orderId";
