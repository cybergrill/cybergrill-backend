/*
  Warnings:

  - You are about to drop the column `orderId` on the `Hamburger` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Hamburger" DROP CONSTRAINT "Hamburger_orderId_fkey";

-- AlterTable
ALTER TABLE "Hamburger" DROP COLUMN "orderId";

-- CreateTable
CREATE TABLE "OrderHamburger" (
    "hamburgerId" INTEGER NOT NULL,
    "orderId" INTEGER NOT NULL,
    "quantity" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "modifiedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "OrderHamburger_pkey" PRIMARY KEY ("orderId","hamburgerId")
);

-- AddForeignKey
ALTER TABLE "OrderHamburger" ADD CONSTRAINT "OrderHamburger_hamburgerId_fkey" FOREIGN KEY ("hamburgerId") REFERENCES "Hamburger"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrderHamburger" ADD CONSTRAINT "OrderHamburger_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "Order"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
