/*
  Warnings:

  - You are about to drop the column `orderId` on the `Additionals` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Additionals" DROP CONSTRAINT "Additionals_orderId_fkey";

-- AlterTable
ALTER TABLE "Additionals" DROP COLUMN "orderId";

-- CreateTable
CREATE TABLE "OrderAdditional" (
    "additionalId" INTEGER NOT NULL,
    "orderId" INTEGER NOT NULL,
    "quantity" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "modifiedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "OrderAdditional_pkey" PRIMARY KEY ("orderId","additionalId")
);

-- CreateTable
CREATE TABLE "OrderDrinks" (
    "drinkId" INTEGER NOT NULL,
    "orderId" INTEGER NOT NULL,
    "quantity" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "modifiedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "OrderDrinks_pkey" PRIMARY KEY ("orderId","drinkId")
);

-- AddForeignKey
ALTER TABLE "OrderAdditional" ADD CONSTRAINT "OrderAdditional_additionalId_fkey" FOREIGN KEY ("additionalId") REFERENCES "Additionals"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrderAdditional" ADD CONSTRAINT "OrderAdditional_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "Order"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrderDrinks" ADD CONSTRAINT "OrderDrinks_drinkId_fkey" FOREIGN KEY ("drinkId") REFERENCES "Drinks"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrderDrinks" ADD CONSTRAINT "OrderDrinks_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "Order"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
